# Complete the can_skydive function so that determines if
# someone can go skydiving based on these criteria
#
# * The person must be greater than or equal to 18 years old, or
# * The person must have a signed consent form

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_skydive(age, has_consent_form):
    # if age is greater than 18 or has form,
    if age >= 18 or has_consent_form:
        # return true
        return True
        # otherwise return false
    else:
        return False


print(can_skydive(16, True))  # Should return True
print(can_skydive(21, False))  # Should return True
print(can_skydive(20, True))  # Should return True
print(can_skydive(16, False))  # Should return False
